$("document").ready(function() {
var game = { step : null, speed : 300, startPosition : -350, colInit : false, score : 0 }
var snake = { segments : 5, dir : "R", canMove : true }
var roaches = { no : 2 };
var mainAnim;
$head = $("#head");
game.step = $head.width();
var windowWidth = $(window).width();
var windowHeight = $( document ).height();

//initiate tail segments
var i = snake.segments;
var position = game.startPosition;
while (i > 0) {
  $('<div id="segment' + i + '"></div>').appendTo('.tail').addClass("segment");
  $segment = $('#segment' + i);
  position += $segment.width();
  $segment.css('left', position + 'px');
  i--;
}
//append the head to the front of the snake's tail
position += $segment.width();
$('#head').css('left', position + 'px');

//initiate roaches
addRoaches(roaches.no);

/* Start the game! */
storePrevPosition();
animateSnake();  

//game controls events
$("body").keydown(function(event) {
      switch (event.keyCode) {
        case 38: //up arrow
        if ( snake.canMove === true ) {
          if (snake.dir !== "D" ) { snake.dir = "U"; };
          snake.canMove = false;
        }
        break;
        
        case 40: //down arrow
        if ( snake.canMove === true ) {
          if (snake.dir !== "U" ) { snake.dir = "D"; };
          snake.canMove = false;snake.canMove = false;
        }
        break;
        
        case 37: //left arrow
        if ( snake.canMove === true ) {
          if (snake.dir !== "R" ) { snake.dir = "L"; };
          snake.canMove = false;
        }
        break;
        
        case 39: //right arrow
        if ( snake.canMove === true ) {
          if (snake.dir !== "L" ) { snake.dir = "R"; };
          snake.canMove = false;
        }
        break;
    }
});

//snake animation loop
function animateSnake() {
    mainAnim = setInterval(function() {
    window.requestAnimationFrame(function() {
      
      //animate the head
      $head = $("#head");
      switch (snake.dir) {
        case "R":
        var pos = parseInt($head.css("left"));
        var newPos = pos + game.step;
        $head.css("left", newPos)
        break
        
        case "L":
        var pos = parseInt($head.css("left"));
        var newPos = pos - game.step;
        $head.css("left", newPos)
        break
        
        case "D":
        var pos = parseInt($head.css("top"));
        var newPos = pos + game.step;
        $head.css("top", newPos)        
        break
        
        case "U":
        var pos = parseInt($head.css("top"));
        var newPos = pos - game.step;
        $head.css("top", newPos)
        break
      }

        //animate the tail
        var i = 1;
        $(".segment").each( function() {
          var segment = $(this);
          var prevSegNo = snake.segments - i;
          if (prevSegNo == 0) {
            var prevSegId = "#head";
          } else {
            var prevSegId = "#segment" + String(prevSegNo);
          }          
          var prevSegPosX = $(prevSegId).data("prevPosX");
          var prevSegPosY = $(prevSegId).data("prevPosY");
          segment.css("left", prevSegPosX + "px")
          segment.css("top", prevSegPosY + "px")
          i++;
        });      
        storePrevPosition();
        snake.canMove = true;
        
        //initiate collision detection (once) when the whole head is on the screen
        if (parseInt($head.css("left")) > $head.width() && game.colInit == false) {
          colDetect();
          game.colInit = true;
        }
     }); //end window request frame
  }, game.speed);
}

//detect collisions
var colDetect = function() {
  setInterval(function() {
    //detect going out of the window
    headPosX = parseInt($head.css("left"));
    headPosY = parseInt($head.css("top"));
    if (headPosX < 0 ||headPosX > windowWidth || headPosY < 0 || headPosY > windowHeight) {
      gameOver();
    }

    //detect hitting the tail
    var $headBox = {
      x: parseInt($('#head').css("left")),
      y: parseInt($('#head').css("top")),
      width: $('#head').width(),
      height: $('#head').height()
    }
    detectClassCollisions($headBox, ".segment", "gameOver();");
    
    //detect eating a roach 
    detectClassCollisions($headBox, ".roaches", "classChild.hide('slow'); classChild.remove(); addSegment(); addSpeed(10); addScore(1000); roaches.no--; if (roaches.no < 1) { addRoaches(1); } ");
         
  }, game.step);
}
	//add score based on the time
    timeScore = setInterval(function() {
		addScore(10);
	}, 1000);

/* Helper Functions */

function addScore(val) {
	game.score +=val;
	$("#score").html("Score: 000" + game.score);
}

function addRoaches(i) {
  var id = $(".roaches").length;
  while (i > 0) {
    $('<div id="roach' + id + '"></div>').appendTo('.enemies').addClass("roaches");
    $roach = $('#roach' + id);
    //make the position a multiply of 25
    var positionX = Math.floor(getRandomInt(1, windowWidth) / game.step) * game.step; 
    var positionY = Math.floor(getRandomInt(1, windowHeight) / game.step) * game.step;
    $roach.css('left', positionX + 'px');
    $roach.css('top', positionY + 'px');
    i--;
    id++;
  }
}

function detectClassCollisions(objBox, className, successCode) {
    $(className).each( function() {
      var classChild = $(this);
      var classChildBox = {
        x: parseInt(classChild.css("left")),
        y: parseInt(classChild.css("top")),
        width: classChild.width(),
        height: classChild.height()
      }
      checkCollision(objBox, classChildBox, function() {
        eval(successCode) ;
        
      });
    });
}


function checkCollision(rect1, rect2, successClosure) {
  if (rect1.x < rect2.x + rect2.width &&
    rect1.x + rect1.width > rect2.x &&
    rect1.y < rect2.y + rect2.height &&
    rect1.height + rect1.y > rect2.y) {
    successClosure();
    }
} 

function storePrevPosition() {
  var i=0;
    $(".segment").each( function() {
      var segment = $(this);
      i++;
      segment.data("prevPosX", parseInt(segment.css("left")));
      segment.data("prevPosY", parseInt(segment.css("top")));
    });
  $head = $("#head");
  $head.data("prevPosX", parseInt($head.css("left")));
  $head.data("prevPosY", parseInt($head.css("top")));
}

function addSpeed(speed) {
      var newspeed = game.speed - speed;
      if (newspeed < 1) { return; } //max speed reached
      clearInterval(mainAnim);
      game.speed = newspeed;
      animateSnake();
}

function addSegment() {
  var i = $(".segment").length;
  var prevSeg = "#segment" + i;
  positionX = parseInt($(prevSeg).css("left"));
  positionY = parseInt($(prevSeg).css("top"));
  i++;
  $('<div id="segment' + i + '"></div>').prependTo('.tail').addClass("segment");
  $segment = $('#segment' + i);
  $segment.css('left', positionX + 'px');
  $segment.css('top', positionY + 'px');
  snake.segments = $(".segment").length;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function gameOver() {
  alert("Game Over\nYour score is " + game.score + "\n Congratulations. Play again.");
  window.location.replace("http://www.iamtomek.com/projects/snake/");
}
});